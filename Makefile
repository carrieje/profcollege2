.PHONY: all

all: test.pdf

test.pdf: test.tex
	pdflatex -shell-escape $<

